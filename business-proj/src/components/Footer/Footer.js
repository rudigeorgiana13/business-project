import React from 'react'
import classes from './Footer.module.css'
import video from '../UI/videos/IT Company commercial Video_ Valiant Filmmaker.mp4'


const Footer = () => {
    return (
      <div className={classes.footerContainer}>

        <div id='outer'>
          <video controls className={classes.video}>
            <source src={video} />
          </video>

        </div>
        <div className={classes.infoContainer}>
          <div className={classes.append}>SAVE MORE TIME</div>
          <div className={classes.h1}>And Boost Productivity</div>
          <div className={classes.description}>  Your employees can bring any success into your
            business, so we need to take care of them
          </div>
          <div className={classes.emailContainer}>
            <input className={classes.input} placeholder={'Email Address'}/>
            <button className={classes.button}> Get started</button>
          </div>
        </div>
    </div>
  )
}

export default Footer