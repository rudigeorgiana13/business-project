import React from 'react'
import classes from './Main.module.css'
import Ellipse1 from '../UI/Icons/Main/Ellipse1.png'
import Ellipse2 from '../UI/Icons/Main/Ellipse2.png'
import Ellipse3 from '../UI/Icons/Main/Ellipse3.png'
import Ellipse4 from '../UI/Icons/Main/Ellipse4.png'
import Ellipse5 from '../UI/Icons/Main/Ellipse5.png'
import Ellipse6 from '../UI/Icons/Main/Ellipse6.png'
import {RiLuggageDepositLine} from 'react-icons/ri'
import {FiSend} from 'react-icons/fi'
import {FiWifiOff} from 'react-icons/fi'
import {BsKanban} from 'react-icons/bs'
import {ImGift} from 'react-icons/im'
import {RiGlobalLine} from 'react-icons/ri'




const Main = () => {
  return (
    <div className={classes.mainContainer}>

      <div className={classes.title}>
        <div className={classes.append}> WORK BETTER </div>
        <div className={classes.h1}> For Your Business </div>
        <div className={classes.description}> <p className={classes.pDescription}> We did research what your company needs and
          here we are providing all of them just for you </p> </div>
      </div>


      <div className={classes.firstAndSecondRowContainer}>
      <div className={classes.firstRow}>

        <div className={classes.section}>
          <div className={classes.sectionIcon}>
            <img src={Ellipse1} className={classes.iconSize}/>
          <RiLuggageDepositLine className={classes.iconLuggage} />
          </div>
          <div className={classes.sectionTitleAndDescription}>
            <div className={classes.sectionTitle}> Share Insights </div>
            <div className={classes.sectionDescription}>  Working together with your
              team to make decisions </div>
          </div>
        </div>

        <div className={classes.section}>
          <div className={classes.sectionIcon}>
            <img src={Ellipse2} className={classes.iconSize}/>
            <FiSend className={classes.sendIcon}/>
          </div>
          <div className={classes.sectionTitleAndDescription}>
            <div className={classes.sectionTitle}> Track Leads </div>
            <div className={classes.sectionDescription}> See where your money goes
              and comes in business </div>
          </div>
        </div>

        <div className={classes.section}>
          <div className={classes.sectionIcon}>
            <img src={Ellipse3} className={classes.iconSize}/>
            <FiWifiOff className={classes.iconWifi}/>
          </div>
          <div className={classes.sectionTitleAndDescription}>
            <div className={classes.sectionTitle}> Offline Mode </div>
            <div className={classes.sectionDescription}> Use the feature while off
              from internet? sure can  </div>
          </div>
        </div>

      </div>


      <div className={classes.secondRow}>
        <div className={classes.section}>
          <div className={classes.sectionIcon}>
            <img src={Ellipse4} className={classes.iconSize}/>
            <BsKanban className={classes.iconKanban} />
          </div>
          <div className={classes.sectionTitleAndDescription}>
            <div className={classes.sectionTitle}> Kanban Mode  </div>
            <div className={classes.sectionDescription}>  Organize the report that
              easy to be understand  </div>
          </div>
        </div>

        <div className={classes.section}>
          <div className={classes.sectionIcon}>
            <img src={Ellipse5} className={classes.iconSize}/>
            <ImGift className={classes.iconPresent}/>
          </div>
          <div className={classes.sectionTitleAndDescription}>
            <div className={classes.sectionTitle}> Reward System </div>
            <div className={classes.sectionDescription}> Motivate your team working
              harder and receive a gift  </div>
          </div>
        </div>

          <div className={classes.section}>
            <div className={classes.sectionIcon}>
              <img src={Ellipse6} className={classes.iconSize}/>
              <RiGlobalLine className={classes.iconGlobal} />
            </div>
            <div className={classes.sectionTitleAndDescription}>
              <div className={classes.sectionTitle}> 189 Country </div>
              <div className={classes.sectionDescription}> Working together worldwide
                people from anywhere </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  )
}

export default Main