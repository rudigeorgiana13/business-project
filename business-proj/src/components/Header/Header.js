import React from 'react'
import classes from './Header.module.css'
import imgGirl from '../UI/Img/Group 4.png'
import analytics from '../UI/Img/Group 7.png'
import  bulk from '../UI/Img/Group 6.png'
import brand from '../UI/Icons/cooperateImg/brand.png'



const Header = () => {
  return (
    <div className={classes.headerContainer}>

      <div className={classes.welcomingContainer}>

        <div className={classes.textWelcomingContainer}>
          <div className={classes.welcomingName}> Manage Payroll Like an Expert  </div>
          <div className={classes.welcomingDescription}> Payna is helping you to setting up the payroll without
            required any finance skills or knowledge before </div>
          <button className={classes.welcomingGetStartButton}> Get Started </button>
        </div>

        <div className={classes.imageContainer}>
          <div> <img src={imgGirl}/></div>
          <div className={classes.analyticsImg}> <img src={analytics}/> </div>
          <div className={classes.bulkImg}> <img src={bulk}/> </div>
        </div>

      </div>

      <div className={classes.cooperatorsContainer}>
        <div className={classes.txt}> Trusted by Global Companies</div>
        <div className={classes.logos}>
          <img src={brand}/>
        </div>
      </div>
    </div>
  )
}

export default Header