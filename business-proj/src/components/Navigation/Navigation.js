import React from 'react'
import {Link} from 'react-router-dom'

import classes from './Navigation.module.css'
import ellipseRed from '../UI/Icons/EllipseRed.png'
import ellipseBlue from '../UI/Icons/EllipseBlue.png'

const Navigation = () => {
  return (
    <nav className={classes.navContainer}>

       <div className={classes.iconLogoContainer}>
        <div className={classes.iconsContainer}>
        <div> <img src={ellipseBlue} className={classes.ellipseBlue} /> </div>
        <div> <img src={ellipseRed}  /> </div>
        </div>
        <p className={classes.logo}> Payna </p>
      </div>

      <div className={classes.links}>
      <Link to="/" className={classes.home}> Home </Link>
      <Link to="/features" className={classes.features}> Features </Link>
      <Link to="/showcase" className={classes.showcase}> Showcase </Link>
      <Link to="/price" className={classes.price}> Price </Link>
      </div>

      <div className={classes.authContainer}>
        <div className={classes.auth} >
          <Link to="/sign-in" className={classes.signIn}> Sign in </Link>
        </div>
      </div>
    </nav>
  )
}


export default Navigation