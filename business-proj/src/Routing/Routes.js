import React from 'react'
import {Route, Routes} from "react-router-dom";
import Home from '../components/Navigation/Nav-Components/Home'
import Features from '../components/Navigation/Nav-Components/Features'
import Pricing from '../components/Navigation/Nav-Components/Pricing'
import Showcase from '../components/Navigation/Nav-Components/Showcase'
import Auth from '../components/Navigation/Nav-Components/Auth'


const Routes = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='/features' element={<Features/>} />
      <Route path='/showcase' element={<Showcase />} />
      <Route path='/price' element={<Pricing />} />
      <Route path='/sign-in' element={<Auth />} />
    </Routes>
  )
}

export default Routes
